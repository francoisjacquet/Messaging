��    "      ,  /   <      �     �                (     1     B  
   T     _     m     r     �     �  	   �     �     �     �     �  
   �     �     �     �     �     �          
  9   )     c     f     m     |     �  :   �  %   �  �  �  #   �  	   	       	        &     8     L     X     k     n     �     �  
   �     �     �     �     �     �  	   �     �     �     �     	          "  H   A     �  	   �     �     �     �  <   �  )   	                                                                     "      
                                 !                    	                              %d new message %d new messages %s Read Archive Archived Archived message Archived messages Back to %s Find a Parent From Message archived. Message sent. Messages Messaging Re: %s Read Read message Read messages Recipients Reply Send Sent Sent message Sent messages Subject The message could not be sent. The message could not be sent. Form elements are missing. To Unread Unread message Unread messages Write You are not allowed to send a message to those recipients. You are not allowed to send messages. Project-Id-Version: Messaging module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-06-05 14:09+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2,3
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %d nuevo mensaje %d nuevos mensajes %s Leído Archivar Archivado Mensaje archivado Mensajes archivados Volver a %s Encontrar un Padre De Mensaje archivado. Mensaje enviado. Mensajes Mensajeria Re: %s Leído Mensaje leído Mensajes leídos Recipientes Responder Enviar Enviado Mensaje enviado Mensajes envidaos Asunto El mensaje no ha sido enviado. El mensaje no ha sido enviado. Elementos del formulario están faltando. Para No leído Mensaje no leído Mensajes no leídos Escribir No tiene permiso para enviar un mensaje a estos recipientes. No está autorizado para enviar mensajes. 