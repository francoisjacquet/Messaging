��    "      ,  /   <      �     �                (     1     B  
   T     _     m     r     �     �  	   �     �     �     �     �  
   �     �     �     �     �     �          
  9   )     c     f     m     |     �  :   �  %   �  �  �  '   �     	               !     2     E     R     d     g     y     �  
   �     �     �  
   �     �     �  	   �     �     �     �     �     
  $     L   5     �     �     �     �     �  F   �  3   �                                                                     "      
                                 !                    	                              %d new message %d new messages %s Read Archive Archived Archived message Archived messages Back to %s Find a Parent From Message archived. Message sent. Messages Messaging Re: %s Read Read message Read messages Recipients Reply Send Sent Sent message Sent messages Subject The message could not be sent. The message could not be sent. Form elements are missing. To Unread Unread message Unread messages Write You are not allowed to send a message to those recipients. You are not allowed to send messages. Project-Id-Version: Messaging module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-06-05 14:09+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2,3
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %d nouveau message %d nouveaux messages %s Lu Archiver Archivé Message archivé Messages archivés Retour au %s Trouver un parent De Message archivé. Message envoyé. Messages Messagerie Re: %s Lu Message lu Messages lus Destinataires Répondre Envoyer Envoyé Message envoyé Messages envoyés Sujet Le message n'a pas pu être envoyé. Le message n'a pas été envoyé. Des éléments du formulaire font défaut. Pour Non lu Message non lu Messages non lus Écrire Vous n'êtes pas autorisé à envoyer un message à ces destinataires. Vous n'êtes pas autorisé à envoyer des messages. 