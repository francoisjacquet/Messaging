Module Messagerie Premium
=========================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_messaging_fr_2021.png)

CARACTÉRISTIQUES
----------------

- Joindre un fichier au message.
- Utiliser les substitutions dans les messages.
- Modèle de message (voir le plugin [Modèles](https://www.rosariosis.org/fr/plugins/templates/) pour gérer plusieurs modèles).
- Lien Envoyer de nouveau (depuis la liste de messages *Envoyés*).
- Me notifier par email lorsque je reçois un message.

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_messaging_fr_2021-300x187.png)](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_messaging_fr_2021.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/messaging_premium_screenshot_fr_2-300.png)](https://www.rosariosis.org/wp-content/uploads/2021/08/messaging_premium_screenshot_fr_2.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/messaging_premium_screenshot_fr_3-300.png)](https://www.rosariosis.org/wp-content/uploads/2021/08/messaging_premium_screenshot_fr_3.png)

### [➭ Passer à Premium](https://www.rosariosis.org/fr/modules/messaging/#premium-module)
