��    !      $  /   ,      �     �     �     �            
   %     0     >     C     U     c  	   l     v     }     �     �  
   �     �     �     �     �     �     �     �  9   �     4     7     >     M     ]  :   c  %   �  �  �  
   �  	   �  
   �     �     �     �     �     
          $  
   8     C     O     \     d     w  
   �     �     �     �     �     �     �  #   �  >         ?  
   B     M     c     y  ,   �  +   �                                                                     !      	                       
                                                             %s Read Archive Archived Archived message Archived messages Back to %s Find a Parent From Message archived. Message sent. Messages Messaging Re: %s Read Read message Read messages Recipients Reply Send Sent Sent message Sent messages Subject The message could not be sent. The message could not be sent. Form elements are missing. To Unread Unread message Unread messages Write You are not allowed to send a message to those recipients. You are not allowed to send messages. Project-Id-Version: Messaging module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:22+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %s Preberi Arhiviraj Arhivirano Sporočilo arhivirano Sporočila arhivirana Nazaj na %s Najdi starša Od Sporočilo arhivirano. Sporočilo poslano. Sporočila Sporočanje Odgovori: %s Preberi Preberi sporočilo Preberi sporočila Prejemniki Odgovori Pošlji Pošlji Pošlji sporočilo Pošlji sporočila Predmet Sporočila ni bilo mogoče poslati. Sporočila ni bilo mogoče poslati. Manjkajo elementi obrazca. Do Neprebrano Neprebrano sporočilo Neprebrana sporočila Pišite Tem prejemnikom ne smete poslati sporočila. Nimate dovoljenja za pošiljanje sporočil. 