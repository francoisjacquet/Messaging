��    !      $  /   ,      �     �     �     �            
   %     0     >     C     U     c  	   l     v     }     �     �  
   �     �     �     �     �     �     �     �  9   �     4     7     >     M     ]  :   c  %   �  �  �     n     v  	        �     �     �     �     �     �     �  	   �  	                       %     3  	   B     L     S     [     l       "   �  C   �     �  	   �     �          %  M   .  0   |                                                                     !      	                       
                                                             %s Read Archive Archived Archived message Archived messages Back to %s Find a Parent From Message archived. Message sent. Messages Messaging Re: %s Read Read message Read messages Recipients Reply Send Sent Sent message Sent messages Subject The message could not be sent. The message could not be sent. Form elements are missing. To Unread Unread message Unread messages Write You are not allowed to send a message to those recipients. You are not allowed to send messages. Project-Id-Version: Messaging module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:22+0200
Last-Translator: Emerson Barros
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %s lido Arquivar Arquivado Mensagem arquivada Mensagens arquivadas Voltar para %s Encontre um pai De Mensagem arquivada. Mensagem enviada. Mensagens Mensagens Re: %s Ler Ler mensagem Ler mensagens Destinatários Responder Enviar Enviado Mensagem enviada Mensagens enviadas Assunto A mensagem não pôde ser enviada. A mensagem não pôde ser enviada. Faltam elementos do formulário. Para Não lido Mensagem não lida Mensagens não lidas Escrever Você não tem permissão para enviar uma mensagem para esses destinatários. Você não tem permissão para enviar mensagens. 