Messaging Premium module
========================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_messaging_en_2021.png)

FEATURES
--------

- Attach File to Message.
- Use Substitutions in Messages.
- Message template (check the [Templates](https://www.rosariosis.org/plugins/templates/) plugin to save various templates).
- Send again link (from *Sent* messages list).
- Notify me by email when I receive a message.

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_messaging_en_2021-300x187.png)](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_messaging_en_2021.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/messaging_premium_screenshot_2-300.png)](https://www.rosariosis.org/wp-content/uploads/2021/08/messaging_premium_screenshot_2.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/messaging_premium_screenshot_3-300.png)](https://www.rosariosis.org/wp-content/uploads/2021/08/messaging_premium_screenshot_3.png)

### [➭ Switch to Premium](https://www.rosariosis.org/modules/messaging/#premium-module)
